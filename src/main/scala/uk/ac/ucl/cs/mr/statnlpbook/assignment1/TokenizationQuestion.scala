package uk.ac.ucl.cs.mr.statnlpbook.assignment1

import java.io.File

import uk.ac.ucl.cs.mr.statnlpbook.Tokenizer
import uk.ac.ucl.cs.mr.statnlpbook.chapter.languagemodels.Util

/**
 * This stub is provided as help for you to solve the first question.
 * @author riedel
 */
object TokenizationQuestion {

  def main(args: Array[String]) {
    //the directory that contains the raw documents
    val dataDir = new File("/Users/mfaizmzaki/Desktop/UCL_courses/Stat_NLP/stat-nlp-book/data/")

    //the file that contains the target gold tokenization
    val goldFile = new File("/Users/mfaizmzaki/Desktop/UCL_courses/Stat_NLP/stat-nlp-book/data/assignment1/p1/goldtokens.txt")

    //the actual raw documents
    val raw = Assignment1Util.loadTokenizationQuestionDocs(dataDir)

    //TODO: The tokenizer you need to improve to match the gold tokenization
    val punct = "[\\.\\?,;!:_]"
    val barStart = "\\[BAR\\]"
    val barEnd = "\\[/BAR\\]"
    val bar = s"((?<=$barStart)|(?=$barEnd)|(?<=$barEnd)(?=$barStart))"
    val simpanTitik = "(Mr|Dr)"
    val beforePunct = s"(?<!$simpanTitik)(?=$punct)"
    val afterPunct = s"(?<=$punct)(?!\\s)"
    val contract = s"(?<=\\w(?=(n\\'t|!n\\'s|\\'ve|\\'d|\\'m|\\'re)))"
    val tokChorus = s"(?<=\\[)(?!(BA)|(/BA))"
    val tokChorusE = s"(?<!R)(?=\\])"
    val sqBrac = s"(?=\\[)|(?<=\\])"
    val doubleQuotes = "(?<=\")|(?=\")"

    val star = s"(?<=\\*)|(?=\\*)"
    val parantheses = s"(?<=(\\(|\\)))|(?=(\\)|\\())"
    val multipleDot = s"(?<=\\.)(?=(\\.|\\w|\\{))"
    val rap1 = s"(?<!n)(?=\\')"
    val rap2 = s"(?<=(in\\b\\'))(?=\\!)"

    val curly1 = s"(?<=\\{)(?=($punct|\\w))"
    val curly2 = s"(?<=(\\w|$punct|\\!))(?=\\})"
    val exclamation = s"(?<=$punct(?=\\!))"
    val exclamation2 = s"(?<=\\w|\\~)(?=\\!)"
    val exclamation3 = s"(?<=\\!)(?=\\')"
    val multiplePunct = s"(?<=$punct)(?=$punct)"
    val multiplePlus = s"(?<=\\+)|(?=\\+)"


    val tokenizer = Tokenizer.fromRegEx(s"(\\s|$multiplePlus|$multiplePunct|$exclamation|$exclamation2|$exclamation3|$curly1|$curly2|$sqBrac|$rap1|$rap2|$multipleDot|$parantheses|$doubleQuotes|$beforePunct|$afterPunct|$star|$contract|$bar|$tokChorus|$tokChorusE)")

    //the result of the tokenization
    val result = Util.words(raw map tokenizer)

    //the gold tokenization file
    val gold = Assignment1Util.loadWords(goldFile).toBuffer

    //we find the first pair of tokens that don't match
    val mismatch = result.zip(gold).find { case (a, b) => a != b }

    //Your goal is to make sure that mismatch is None
    mismatch match {
      case None => println("Success!")
      case Some(pair) => println("The following tokens still don't match: " + pair)
    }
  }

}
