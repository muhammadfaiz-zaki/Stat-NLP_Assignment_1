package uk.ac.ucl.cs.mr.statnlpbook.assignment1

import java.io.File

import uk.ac.ucl.cs.mr.statnlpbook.chapter.languagemodels._

import scala.collection.mutable
import scala.collection.mutable.HashMap
import scala.collection.mutable.Buffer
import java.io.PrintWriter

/**
 * @author riedel
 */
object BarQuestion {

  /**
   * An LM that assigns increasing probability to the [/BAR] token the further the last [/BAR] token is away,
   * and uniform probability for all other tokens. How to choose the function that maps [/BAR] distance to a probability
   * is left to you, but you should guide your choice by the goal of minimizing perplexity.
   * @param vocab the vocabulary.
   */
  case class MyBarAwareLM(vocab: Set[String]) extends LanguageModel {
    def order = 20

    //TODO: This needs to be improved by you.
    def probability(word: String, history: String*) = {
   
      val tokenDistance = (history.length - (history.lastIndexOf("[BAR]") + 1)).toDouble

      var probZeroDistance = 0.0

      if (tokenDistance == 0) {
        probZeroDistance = 0.07822
      } else {
        probZeroDistance = 0.0
      }

      //probability based on a linear equation
      val probClosedBar = (0.015 * tokenDistance) + probZeroDistance

      if (vocab(word)) {
        if (word == "[/BAR]") {
          probClosedBar
        } else {
          (1 - probClosedBar) / (vocab.size - 1)

        }
      } else {
        0.0
      }
    }
  }

 case class PerDistanceLM(trainFile:Buffer[String], vocab: Set[String]) extends LanguageModel{
    
    def order = 20

    def probability(word: String, history: String*) = {

    val counts = new HashMap[Double, Double] withDefaultValue 0.0

    var count = 0

    for (word <- trainFile) {
      if (word == "[/BAR]") {
        counts(count) += 1
      } else if (word == "[BAR]") {
        count = 0
      } else {
        count += 1
      }
    }

    val totalSentence = counts.foldLeft(0.0)((z,el) => z + el._2)
  
    //probability of each of the token distance
    val probEndBar = counts.map(x => x._1 -> x._2 / totalSentence)

    val tokenDistance = (history.length - (history.lastIndexOf("[BAR]") + 1)).toDouble    

    
    //extracting the probability from HashMap (counts) based on token distance
    val probPerDistance = probEndBar(tokenDistance)

    //probability of seeing [/BAR] in a given token distance
    val probEndBarLength = ((history.length - history.lastIndexOf("[BAR]"))/(history.length + 1.0)).toDouble
    
    //probability of seeing [/BAR] in the probability of a given distance
    val probEndBarPerDistance = probEndBarLength * probPerDistance

      if (vocab(word)) 
    {
      if (word == "[/BAR]") 
      {
        probEndBarPerDistance 
      } 
      else if (word == "[BAR]")  
      {
        if (history.size == 0 || history.last == "[/BAR]") 
        {
            1.0
        } 
        else 
        {
            0.0
        }
      } 
      else 
      {
          (1 - probEndBarPerDistance) / (vocab.size - 1) 
      }
    } 
    else 
    {
        0.0
    }    
 
 }
 
 }

case class InterpolatedLM(main:LanguageModel, backoff:LanguageModel, alpha:Double) extends LanguageModel {
  def order = main.order
  def vocab = main.vocab
  def probability(word:String, history:String*) = 
  {
    alpha * main.probability(word,history:_*) +
    (1 - alpha) * backoff.probability(word, history.drop(1):_*)
  }
}

  case class UniformLM(vocab: Set[String]) extends LanguageModel {
    def order = 1

    def probability(word: String, history: String*) ={
    
      if (vocab(word)) 1.0 / vocab.size else 0.0
    }
  }


  def main(args: Array[String]) {
    //The training file we provide you
    val trainFile = new File("/Users/mfaizmzaki/Desktop/UCL_courses/Stat_NLP/stat-nlp-book/data/assignment1/p2/p2_train.txt")

    //the dev file we provide you.
    val devFile = new File("/Users/mfaizmzaki/Desktop/UCL_courses/Stat_NLP/stat-nlp-book/data/assignment1/p2/p2_dev.txt")

    //the training sequence of words
    val train =  Assignment1Util.loadWords(trainFile).toBuffer
    
    //the dev sequence of words
    val dev = Assignment1Util.loadWords(devFile).toBuffer
  
    //the vocabulary. Contains the training words and the OOV symbol (as the dev set has been preprocessed by
    //replacing words not in the training set with OOV).
    val vocab = train.toSet + Util.OOV

  //TODO: Improve the MyBarAwareLM implementation
   val lm = MyBarAwareLM(vocab)
   val pdLM = PerDistanceLM(train,vocab)
   val unigram = UniformLM(vocab)
   val interpolated = InterpolatedLM(pdLM,unigram,15.0)
   
   //This calculates the perplexity of lm
   val pp1 = LanguageModel.perplexity(lm, dev)
   val pp2= LanguageModel.perplexity(pdLM, dev)
   val pp3 = LanguageModel.perplexity(interpolated, dev)
  
  println("Perplexity of Linearly Bar-Aware: " + pp1)
  println("Perplexity of Per-Distance Bar-Aware: " + pp2)
  println("Perplexity of Interpolated LM: " + pp3)
      

  }
}
