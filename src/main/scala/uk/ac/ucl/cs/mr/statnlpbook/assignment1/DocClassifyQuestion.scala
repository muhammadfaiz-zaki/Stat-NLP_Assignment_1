package uk.ac.ucl.cs.mr.statnlpbook.assignment1

import java.io.File
import uk.ac.ucl.cs.mr.statnlpbook.assignment1.Assignment1Util.Instance
import uk.ac.ucl.cs.mr.statnlpbook.chapter.languagemodels.{UniformLM, LanguageModel}


/**
 * @author mbosnjak
 */

object DocClassifyQuestion {



  def main(args: Array[String]): Unit = {
    
    // load the datasets
     val train = Assignment1Util.loadDataset(new File("data/assignment1/p3/p3_train.txt"))
     val dev = Assignment1Util.loadDataset(new File("data/assignment1/p3/p3_dev.txt"))

    //val test = Assignment1Util.loadDataset(new File(args(2)))

    val length = train.length
    var countRoots = 0.0
    var countRakim = 0.0
    var countJ_Live = 0.0

     for(i <- 0 until length){

        if(train(i).author.get == "roots"){
            countRoots += 1.0
        }
        else if(train(i).author.get == "rakim"){
            countRakim += 1.0
        }else countJ_Live += 1.0
    }
    val sumofAll = countJ_Live + countRakim + countRoots

    //probability of each artist
    val probOfRoots = (countRoots / sumofAll).toDouble
    val probOfRakim = (countRakim / sumofAll).toDouble
    val probOfJ_Live = (countJ_Live / sumofAll).toDouble
    
    var rootsLyrics = Map[String, Double]()
    var rakimLyrics = Map[String, Double]()
    var j_liveLyrics = Map[String, Double]()

    //total per words on all lyrics for each artist
    for(i <- 0 until length){
        if(train(i).author.get == "roots") {
           rootsLyrics= train(i).lyrics.split(" ").foldLeft(Map.empty[String, Double]){
        (count, word) => count + (word -> (count.getOrElse(word, 0.0) + 1.0))
            }
        }
        else if(train(i).author.get == "rakim"){
            rakimLyrics = train(i).lyrics.split(" ").foldLeft(Map.empty[String, Double]){
        (count, word) => count + (word -> (count.getOrElse(word, 0.0) + 1.0))
            }
        }else j_liveLyrics = train(i).lyrics.split(" ").foldLeft(Map.empty[String, Double]){
        (count, word) => count + (word -> (count.getOrElse(word, 0.0) + 1.0))
            }
    }

    //"vocab" for each artist
    val vocRoots = collection.mutable.Map((rakimLyrics.map(x => x._1 -> x._2 * 0.0) ++ j_liveLyrics.map(x => x._1 -> x._2 * 0.0) ++ rootsLyrics).toSeq: _*)
    val vocRakim = collection.mutable.Map((rootsLyrics.map(x => x._1 -> x._2 * 0.0) ++ j_liveLyrics.map(x => x._1 -> x._2 * 0.0) ++ rakimLyrics).toSeq: _*)
    val vocJ_Live = collection.mutable.Map((rootsLyrics.map(x => x._1 -> x._2 * 0.0) ++ rakimLyrics.map(x => x._1 -> x._2 * 0.0) ++ j_liveLyrics).toSeq: _*)

    //smoothing
    for((k,v) <- vocRoots) vocRoots(k) += 1.0
    for((k,v) <- vocRakim) vocRakim(k) += 1.0
    for((k,v) <- vocJ_Live) vocJ_Live(k) += 1.0

    //total words for all lyrics of each artist
    val totalRoots = vocRoots.foldLeft(0.0)((z,el) => z + el._2)
    val totalRakim = vocRakim.foldLeft(0.0)((z,el) => z + el._2)    
    val totalJ_live = vocJ_Live.foldLeft(0.0)((z,el) => z + el._2)

    //probability of each word in each artist lyrics
    val probWordsRoots = vocRoots.map(word => word._1 -> word._2 / totalRoots)
    val probWordsRakim = vocRakim.map(word => word._1 -> word._2 / totalRakim)
    val probWordsJ_Live = vocJ_Live.map(word => word._1 -> word._2 / totalJ_live)

    //initial prediction probability
    var predictRoots = 0.0
    var predictRakim = 0.0
    var predictJ_Live = 0.0


 def classify(instance: Instance): Option[String]= {       
// TODO given an train, how would you classify it   
    
    predictRoots = (instance.toString.split(" ") collect vocRoots).foldLeft(1.0)(_*_)
    predictRoots = (instance.toString.split(" ") collect vocRakim).foldLeft(1.0)(_*_)
    predictRoots = (instance.toString.split(" ") collect vocJ_Live).foldLeft(1.0)(_*_)
    val predictRootsFinal = predictRoots*probOfRoots
    val predictRakimFinal = predictRakim*probOfRakim
    val predictJ_LiveFinal = predictJ_Live*probOfJ_Live

    if(predictRootsFinal > predictRakimFinal && predictRootsFinal > predictJ_LiveFinal){
        return Some("roots")
      }else if(predictRakimFinal > predictRootsFinal && predictRakimFinal > predictJ_LiveFinal){
        return Some("rakim")
      }else if(predictJ_LiveFinal > predictRootsFinal && predictJ_LiveFinal > predictRakimFinal){
       return Some("j_live")
    }else return Some("none")


}   

    //execute your classifier
     val predictions = dev.map(i => Instance(i.lyrics, i.author, classify(i)))

    // accurately predicted instances
    val accuratelyPredicted = predictions.map(i => i.author.get == i.prediction.get).count(_ == true)

    // total number of instances
    val totalInstances = predictions.length

    // evaluate accuracy
    val accuracy = 1.0 * accuratelyPredicted / totalInstances

    println("classification accuracy:" + accuracy)

    }
}

   






  




